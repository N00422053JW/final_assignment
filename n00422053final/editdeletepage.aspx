﻿<%@ Page Title="home" Language="C#" MasterPageFile="~/mainpage.Master" AutoEventWireup="true" CodeBehind="editdeletepage.aspx.cs" Inherits="n00422053final.editdeletepage" %>
<asp:Content runat="server" ContentPlaceHolderID="editdeletepage_content">
<asp:SqlDataSource ConnectionString="<%$ ConnectionStrings:final_sql_con%>" runat="server" id="page_editdelete"></asp:SqlDataSource>
    
<form id="addpageform" runat="server">
<h2>EDIT YOUR PAGE</h2>

<p>Page Title:</p>
    <asp:TextBox id="page_title" runat="server"/>
<p>Page Content:</p>
    <asp:TextBox id="page_content" runat="server" TextMode="multiline" Columns="50" Rows="10"/>
<br/>
<asp:button runat="server" Text="Save" OnClick="save_Page"/>
<asp:button runat="server" Text="Delete" OnClick="delete_Page"/>
        
</form> 
    
</asp:Content>