﻿<%@ Page Language="C#" MasterPageFile="~/mainpage.master" AutoEventWireup="true" CodeFile="home.aspx.cs" Inherits="n00422053final.home"%>
<asp:Content runat="server" ContentPlaceHolderID="home_content">
    
<asp:SqlDataSource ConnectionString="<%$ ConnectionStrings:final_sql_con%>" runat="server" id="page_select"></asp:SqlDataSource>

<asp:DataGrid runat="server" id="page_list" CssClass="grid" GridLines="None" CellPadding="3" Width="800px">
    <HeaderStyle Font-Size="Large" BackColor="#ffb655" ForeColor="White"/>
    <ItemStyle BackColor="#ffffff" ForeColor="#2e5077"/> 
</asp:DataGrid>

    
</asp:Content>