﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace n00422053final
{
    public partial class mainpage : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string query = "select * from pages";
            page_select.SelectCommand = query;
            DataView pageview = (DataView)page_select.Select(DataSourceSelectArguments.Empty);
            DataRowView pagerowview;
            int view_length = pageview.ToTable().Rows.Count;
            listOfPages.InnerHtml = "<li><a href =" + "home.aspx" + ">HOME</a></li>";
            for (int i = 0; i < view_length; i++)
            {
                pagerowview = pageview[i];
                listOfPages.InnerHtml += "<li><a href=" + "/Pages.aspx?pageid=" + pagerowview["pageid"] + ">" + pagerowview["pagetitle"].ToString() + "</a></li> ";
            }
        }
    }
}