﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="editPage1.ascx.cs" Inherits="n00422053final.editPage" %>
<asp:SqlDataSource runat="server" ID="page1_edit" ConnectionString="<%$ ConnectionStrings:final_sql_con%>">
</asp:SqlDataSource>

<p>Page Title:</p>
<asp:TextBox id="editpagetitle" Placeholder="Enter here" runat="server"/>
<asp:RequiredFieldValidator runat="server" ErrorMessage="Please edit title here." ControlToValidate="editpagetitle" id="validatorName"></asp:RequiredFieldValidator>

<p>Page Content:</p>
<asp:TextBox id="editpagecontent" Placeholder="Enter here" runat="server"/>
<asp:RequiredFieldValidator runat="server" ErrorMessage="Please edit content here." ControlToValidate="editpagecontent" id="validatorName"></asp:RequiredFieldValidator>
