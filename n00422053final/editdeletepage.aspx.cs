﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace n00422053final
{
    public partial class editdeletepage : System.Web.UI.Page
    {
        public string pageid
        {
            get { return Request.QueryString["pageid"]; }
        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (pageid == "" || pageid == null)
            {
                page_title.Text = "";
                page_content.Text = "";
            }
            else
            {
                DataRowView pagerow = getPageInfo(Int32.Parse(pageid));
                page_title.Text = pagerow["pagetitle"].ToString();
                page_content.Text = pagerow["pagecontent"].ToString();
            }
        }
        protected DataRowView getPageInfo(int id)
        {
            string query = "select * from pages where pageid=" + pageid;
            page_editdelete.SelectCommand = query;
            DataView pageview = (DataView)page_editdelete.Select(DataSourceSelectArguments.Empty);

            if (pageview.ToTable().Rows.Count < 1)
            {
                return null;
            }
            DataRowView pagerowview = pageview[0];
            return pagerowview;
        }
        protected void save_Page(object sender, EventArgs e)
        {
            string title = page_title.Text;
            string content = page_content.Text;
            string query = "UPDATE PAGES SET pagetitle = '"+title+"',pagecontent= '"+content+"' WHERE PAGEID = " + pageid;
            page_editdelete.UpdateCommand = query;
            page_editdelete.Update();
        }
        protected void delete_Page(object sender, EventArgs e)
        {
            string query = "DELETE FROM PAGES WHERE pageid = "+ pageid;
            page_editdelete.DeleteCommand = query;
            page_editdelete.Delete();

            Response.Redirect("home.aspx");
        }
}
}