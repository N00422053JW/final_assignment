﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace n00422053final
{

    public partial class home : System.Web.UI.Page
    {
        private string query = "SELECT * FROM PAGES";

        protected void Page_Load(object sender, EventArgs e)
        {

            page_select.SelectCommand = query;
            page_list.DataSource = pages_Manual_Bind(page_select);

            page_list.DataBind();
        }

        protected DataView pages_Manual_Bind(SqlDataSource src)
        {
            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;

            DataColumn editcol = new DataColumn();
            editcol.ColumnName = "Action";
            editcol.DefaultValue = "EDIT/DELETE";

            mytbl.Columns.Add(editcol);

            foreach (DataRow row in mytbl.Rows)
            {
                row[editcol] =
                    "<a href=\"editdeletepage.aspx?pageid="
                    + row["pageid"]
                    + "\">"
                    + row[editcol]
                    + "</a>";
            }
            mytbl.Columns.Remove("pageid");
            myview = mytbl.DefaultView;

            return myview;

        }
    }
}