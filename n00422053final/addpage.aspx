﻿<%@ Page Title="home" Language="C#" MasterPageFile="~/mainpage.Master" AutoEventWireup="true" CodeBehind="addpage.aspx.cs" Inherits="n00422053final.addpage" %>
<asp:Content runat="server" ContentPlaceHolderID="addpage_content">
<asp:SqlDataSource ConnectionString="<%$ ConnectionStrings:final_sql_con%>" runat="server" id="page_insert"></asp:SqlDataSource>
    
<form id="addpageform" runat="server">
<h2>ADD NEW PAGE</h2>

<p>Page Title:</p>
    <asp:TextBox id="page_title" Placeholder="Please enter title here." runat="server"/>
    <br/>
    <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a title." ControlToValidate="page_title" id="validatorTitle"></asp:RequiredFieldValidator>
<p>Page Content:</p>
    <asp:TextBox id="page_content" Placeholder="Please enter content here." runat="server" TextMode="multiline" Columns="80" Rows="10"/>
    <br/>
        <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter content." ControlToValidate="page_content" id="validatorContent"></asp:RequiredFieldValidator>
<br/>
<asp:button runat="server" Text="Add" OnClick="Add_Page"/>
</form>  
    <div id="debug" runat="server"></div>
</asp:Content>